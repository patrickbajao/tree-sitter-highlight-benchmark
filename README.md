# Tree-sitter Highlight Benchmark

## Requirement

Rust with nightly for running bench

## Benchmark

1. Run `git submodule init`.
1. Run `git submodule update`.
1. Run `cargo +nightly bench`.
