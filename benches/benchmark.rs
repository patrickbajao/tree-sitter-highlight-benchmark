#![feature(test)]
extern crate test;

use std::fs::File;
use std::io::prelude::*;
use std::path::Path;
use test::Bencher;

#[path = "../highlighter.rs"]
mod highlighter;

#[bench]
fn json_benchmark(b: &mut Bencher) {
    // Create a path to the desired file
    let path = Path::new("index.json");
    let display = path.display();

    // Open the path in read-only mode, returns `io::Result<File>`
    let mut file = match File::open(&path) {
        Err(why) => panic!("couldn't open {}: {}", display, why),
        Ok(file) => file,
    };

    // Read the file contents into a string, returns `io::Result<usize>`
    let mut s = String::new();
    match file.read_to_string(&mut s) {
        Err(why) => panic!("couldn't read {}: {}", display, why),
        Ok(s) => s,
    };

    let raw_code = s.clone();
    let scope = "source.json";
    b.iter(|| {
        highlighter::highlight(&raw_code, scope);
    });
}

#[bench]
fn ruby_benchmark(b: &mut Bencher) {
    // Create a path to the desired file
    let path = Path::new("merge_request.rb");
    let display = path.display();

    // Open the path in read-only mode, returns `io::Result<File>`
    let mut file = match File::open(&path) {
        Err(why) => panic!("couldn't open {}: {}", display, why),
        Ok(file) => file,
    };

    // Read the file contents into a string, returns `io::Result<usize>`
    let mut s = String::new();
    match file.read_to_string(&mut s) {
        Err(why) => panic!("couldn't read {}: {}", display, why),
        Ok(s) => s,
    };

    let raw_code = s.clone();
    let scope = "source.ruby";
    b.iter(|| {
        highlighter::highlight(&raw_code, scope);
    });
}
