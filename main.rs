use std::fs::File;
use std::io::prelude::*;
use std::path::Path;

mod highlighter;

fn main() {
    // Create a path to the desired file
    let path = Path::new("merge_request.rb");
    let display = path.display();

    // Open the path in read-only mode, returns `io::Result<File>`
    let mut file = match File::open(&path) {
        Err(why) => panic!("couldn't open {}: {}", display, why),
        Ok(file) => file,
    };

    // Read the file contents into a string, returns `io::Result<usize>`
    let mut s = String::new();
    match file.read_to_string(&mut s) {
        Err(why) => panic!("couldn't read {}: {}", display, why),
        Ok(s) => s,
    };

    let raw_code = s;
    let scope = "source.ruby";
    let highlighted_code = highlighter::highlight(&raw_code, scope);
    println!("{}", highlighted_code);
}
