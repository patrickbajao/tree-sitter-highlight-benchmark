use tree_sitter_cli::highlight::Theme;
use tree_sitter_highlight::{Highlight, Highlighter, HtmlRenderer};
use tree_sitter_loader::{Config, Loader};

pub fn highlight(code: &str, scope: &str) -> String {
    // The directory to search for parsers
    let parser_directory = std::env::current_dir()
        .unwrap()
        .join("parsers");

    let theme = Theme::default();

    // The loader is used to load parsers
    let loader = {
        let mut loader = Loader::new().unwrap();
        let config = {
            let parser_directories = vec![parser_directory];
            Config { parser_directories }
        };
        loader.find_all_languages(&config).unwrap();
        loader.configure_highlights(&theme.highlight_names);
        loader
    };

    // Retrieve the highlight config for the given language scope
    let config = loader
        .language_configuration_for_scope(scope)
        .unwrap()
        .and_then(|(language, config)| config.highlight_config(language).ok())
        .unwrap()
        .unwrap();

    let code = code.as_bytes();

    // Highlight the code
    let mut highlighter = Highlighter::new();
    let events = highlighter.highlight(config, code, None, |_| None).unwrap();

    let get_css_style = |h: Highlight| {
        if let Some(style) = &theme.styles.get(h.0) {
            if let Some(css) = &style.css {
                css.as_bytes()
            } else {
                "".as_bytes()
            }
        } else {
            "".as_bytes()
        }
    };

    // Render HTML
    let mut renderer = HtmlRenderer::new();
    renderer.render(events, code, &get_css_style).unwrap();
    renderer.lines().collect()
}
